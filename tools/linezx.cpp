#include <common_utils.h>

#include <algorithm>
#include <charconv>
#include <cstdlib>
#include <random>
#include <string_view>

enum class Mode {
    Invalid,
    Utf8CollateSort,
    StrcmpSort,
    PrandShuffle,
    CQuotedList,
    Repeat
};

struct ModeArg {
    std::string_view str;
    Mode mode;
};

static const ModeArg ModeArgs[] = {
        {"utf8-collate-sort", Mode::Utf8CollateSort},
        {"strcmp-sort", Mode::StrcmpSort},
        {"prand-shuffle", Mode::PrandShuffle},
        {"c-quoted-list", Mode::CQuotedList},
        {"repeat", Mode::Repeat},
    };

[[noreturn]] static void usage()
{
    fprintf(stderr, "%s", u8R"(linezx - read lines from stdin, modify according to mode, and write to stdout

Empty lines are removed in all modes.

Usage:

linezx utf8-collate-sort LOCALE
  - sort lines with icu collation and given locale
linezx strcmp-sort
  - sort with strcmp (i.e. lexicographical on the unsigned byte values)
linezx prand-shuffle [SEED=0]
  - shuffle pseudo-randomly; guaranteed to yield the same deterministic shuffling for a given seed and line count
linezx c-quoted-list
  - turn the lines to a quoted c string list
linezx repeat [COUNT=2]
  - repeat lines COUNT times
)");
    std::exit(EXIT_FAILURE);
}

struct c_quote_lines : public file_line_reader {
    bool first = true;

    void operator()(std::string&& s) override {
        for( size_t i = 0; i < s.size(); ++i ) {
            switch( s[i] ) {
            case '\"':
            case '\\':
                s.insert(i, 1, '\\');
                ++i;
                break;
            }
        }

        fprintf(stdout, first ? "\"%s\"" : ",\n\"%s\"", s.data());
        first = false;
    }
};

struct repeat_lines : public file_line_reader {
    unsigned count = 999;

    void operator()(std::string&& s) override {
        for( unsigned i = 0; i < count; ++i )
            fprintf(stdout, "%s\n", s.data());
    }
};

int main(int argc, char* argv[])
{
    if( argc < 2 || argc > 3 )
        usage();

    std::string_view arg_mode(argv[1]);
    Mode mode = Mode::Invalid;
    for( const auto& m : ModeArgs ) {
        if( m.str == arg_mode )
            mode = m.mode;
    }

    unsigned arg_number = (mode == Mode::Repeat ? 2 : 0);
    switch( mode ) {
    case Mode::Invalid:
        usage();
    case Mode::Utf8CollateSort:
        if( argc != 3 )
            usage();
        break;
    case Mode::StrcmpSort:
    case Mode::CQuotedList:
        if( argc != 2 )
            usage();
        break;
    case Mode::PrandShuffle:
    case Mode::Repeat:
        if( argc == 3 ) {
            std::string_view arg_numstr(argv[2]);
            const auto r = std::from_chars(arg_numstr.begin(), arg_numstr.end(), arg_number);
            if( r.ec != std::errc() || r.ptr != arg_numstr.end() )
                usage();
        }
        break;
    }

    switch( mode ) {
    case Mode::Invalid:
        std::abort();
    case Mode::Utf8CollateSort:
    case Mode::StrcmpSort:
    case Mode::PrandShuffle: {
        file_lines_to_strvec r;
        r.read(stdin);
        if( mode == Mode::Utf8CollateSort ) {
            utf8_string_collator c(argv[2]);
            std::sort(r.lines.begin(), r.lines.end(), c.get_comparator());
        } else if( mode == Mode::StrcmpSort ) {
            std::sort(r.lines.begin(), r.lines.end());
        } else {
            std::minstd_rand gen;
            gen.seed(arg_number);
            shuffle(r.lines.begin(), r.lines.end(), &gen);
        }
        for( const auto& l : r.lines )
            fprintf(stdout, "%s\n", l.data());
    } break;
    case Mode::CQuotedList: {
        c_quote_lines r;
        r.read(stdin);
        fprintf(stdout, "\n");
    } break;
    case Mode::Repeat: {
        repeat_lines r;
        r.count = arg_number;
        r.read(stdin);
    } break;
    }

    return EXIT_SUCCESS;
}
