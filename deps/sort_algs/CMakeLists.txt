
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/pdqsort/pdqsort.h)
    message(FATAL_ERROR "Use 'git submodule update --init' to make bundled deps available")
endif()

set(SORT_ALGS_INCLUDE_DIRS
cpp-TimSort/include
pdqsort
ska_sort
)

add_library(sort_algs_wrapper INTERFACE)
target_compile_features(sort_algs_wrapper INTERFACE cxx_std_20)
target_include_directories(sort_algs_wrapper INTERFACE ${SORT_ALGS_INCLUDE_DIRS})
