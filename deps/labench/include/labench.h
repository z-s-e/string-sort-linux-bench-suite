#ifndef LIBLABENCH_H
#define LIBLABENCH_H

#include <time.h>
#include <sys/resource.h>

#define LIBLABENCH_EXPORT __attribute__((visibility("default")))

class LIBLABENCH_EXPORT labench {
public:
    enum WarningFlags : unsigned {
        WarningDebugBuild = 1<<0
    };

    inline void benchmark()
    {
#ifdef NDEBUG
        benchmark_impl(false);
#else
        benchmark_impl(true);
#endif
    }

    virtual ~labench();

protected:
    virtual const char* name();
    virtual void start_measure();
    virtual void run() = 0;
    virtual void stop_measure();
    virtual void report(unsigned warning_flags);

    timespec monotonic_start = {};
    timespec monotonic_stop = {};
    rusage thread_start = {};
    rusage thread_stop = {};
    rusage self_start = {};
    rusage self_stop = {};

private:
    void benchmark_impl(bool is_debug);
};

#endif
