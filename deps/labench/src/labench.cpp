#include "labench.h"

#include <chrono>
#include <cstdlib>
#include <stdio.h>
#include <typeinfo>


static std::chrono::nanoseconds to_chrono(timespec ts)
{
    return std::chrono::nanoseconds(ts.tv_nsec) + std::chrono::seconds(ts.tv_sec);
}

static std::chrono::microseconds to_chrono(timeval tv)
{
    return std::chrono::microseconds(tv.tv_usec) + std::chrono::seconds(tv.tv_sec);
}

void labench::benchmark_impl(bool is_debug)
{
    static bool already_running = false;

    if( already_running )
        std::abort();
    already_running = true;

    start_measure();
    if( clock_gettime(CLOCK_MONOTONIC, &monotonic_start) != 0 )
        std::abort();
    run();
    if( clock_gettime(CLOCK_MONOTONIC, &monotonic_stop) != 0 )
        std::abort();
    stop_measure();

    unsigned warnings = 0;
    if( is_debug )   warnings |= WarningDebugBuild;

    report(warnings);

    already_running = false;
}

const char* labench::name()
{
    return typeid(*this).name();
}

void labench::start_measure()
{
    if( getrusage(RUSAGE_THREAD, &thread_start) != 0 )
        std::abort();
    if( getrusage(RUSAGE_SELF, &self_start) != 0 )
        std::abort();
}

void labench::stop_measure()
{
    if( getrusage(RUSAGE_THREAD, &thread_stop) != 0 )
        std::abort();
    if( getrusage(RUSAGE_SELF, &self_stop) != 0 )
        std::abort();
}

void labench::report(unsigned warning_flags)
{
    auto round = [] (std::chrono::nanoseconds ns) { return long(std::chrono::round<std::chrono::milliseconds>(ns).count()); };

    printf("> %40s : %6ld ms  (user: %6ld / %6ld  --  sys: %6ld / %6ld)\n",
           name(),
           round(to_chrono(monotonic_stop) - to_chrono(monotonic_start)),
           round(to_chrono(thread_stop.ru_utime) - to_chrono(thread_start.ru_utime)),
           round(to_chrono(self_stop.ru_utime) - to_chrono(self_start.ru_utime)),
           round(to_chrono(thread_stop.ru_stime) - to_chrono(thread_start.ru_stime)),
           round(to_chrono(self_stop.ru_stime) - to_chrono(self_start.ru_stime)));

    auto print_warning = [] (const char* warning) { printf("! WARNING: %s\n", warning); };

    if( warning_flags & WarningDebugBuild )
        print_warning("Benchmarking debug build");
}

labench::~labench() = default;
