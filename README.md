Some performance experiments regarding string sorting operations.

The contained benchmarks need to be supplied with a string list file. Generate one with e.g. `find / > test.txt`.
