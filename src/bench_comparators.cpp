#include <common_utils.h>
#include <labench.h>

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <numeric>
#include <random>
#include <unicode/unistr.h>


struct common_bench : public labench {
    std::vector<std::string>* work = nullptr;

    void setup_and_bench(const std::vector<std::string>& input, std::vector<std::string>& w) {
        w = input;
        work = &w;
        benchmark();
    }
};

struct strcmp_comparator : public common_bench {
    void run() override {
        std::sort(work->begin(), work->end(), [] (const std::string& lhs, const std::string& rhs) {
            return std::strcmp(lhs.data(), rhs.data()) < 0;
        });
    }
};

struct strcmp_on_raw_ptr_vec : public labench {
    std::vector<const char*> work;

    strcmp_on_raw_ptr_vec(const std::vector<std::string>& input) {
        work.reserve(input.size());
        for( const auto& s : input )
            work.emplace_back(s.data());
    }

    void run() override {
        std::sort(work.begin(), work.end(), [] (const char* lhs, const char* rhs) {
            return std::strcmp(lhs, rhs) < 0;
        });
    }
};

struct strcmp_on_string_view_vec : public labench {
    std::vector<std::string_view> work;

    strcmp_on_string_view_vec(const std::vector<std::string>& input) {
        work.reserve(input.size());
        for( const auto& s : input )
            work.emplace_back(s);
    }

    void run() override {
        std::sort(work.begin(), work.end(), [] (const std::string_view& lhs, const std::string_view& rhs) {
            return std::strcmp(lhs.data(), rhs.data()) < 0;
        });
    }
};

struct strcmp_on_unique_ptr_vec : public labench {
    std::vector<std::unique_ptr<char[]>> work;

    strcmp_on_unique_ptr_vec(const std::vector<std::string>& input) {
        work.reserve(input.size());
        for( const auto& s : input ) {
            auto tmp = new char[s.size() + 1];
            std::memcpy(tmp, s.data(), s.size() + 1);
            work.emplace_back(tmp);
        }
    }

    void run() override {
        std::sort(work.begin(), work.end(), [] (const std::unique_ptr<char[]>& lhs, const std::unique_ptr<char[]>& rhs) {
            return std::strcmp(lhs.get(), rhs.get()) < 0;
        });
    }
};

struct strcmp_on_reorder_vec : public labench {
    const std::vector<std::string>& work;
    std::vector<unsigned> reorder;

    strcmp_on_reorder_vec(const std::vector<std::string>& input) : work(input) {
        reorder.resize(work.size());
    }

    void run() override {
        std::iota(reorder.begin(), reorder.end(), 0);

        std::sort(reorder.begin(), reorder.end(), [this] (unsigned lhs, unsigned rhs) {
            return std::strcmp(work[lhs].data(), work[rhs].data()) < 0;
        });
    }
};

struct memcmp_comparator : public common_bench {
    void run() override {
        std::sort(work->begin(), work->end(), [] (const std::string& lhs, const std::string& rhs) {
            const auto s1 = lhs.size();
            const auto s2 = rhs.size();
            auto r = std::memcmp(lhs.data(), rhs.data(), std::min(s1, s2));
            return r != 0 ? (r < 0) : (s1 < s2);
        });
    }
};

struct len_memcmp_comparator : public common_bench {
    void run() override {
        std::sort(work->begin(), work->end(), [] (const std::string& lhs, const std::string& rhs) {
            const auto s1 = lhs.size();
            const auto s2 = rhs.size();
            if( s1 != s2 )
                return s1 < s2;
            return std::memcmp(lhs.data(), rhs.data(), s1) < 0;
        });
    }
};

struct stdcmp_comparator : public common_bench {
    void run() override {
        std::sort(work->begin(), work->end());
    }
};

struct icu_utf8_comparator : public common_bench {
    utf8_string_collator col;

    void run() override {
        std::sort(work->begin(), work->end(), col.get_comparator());
    }
};

struct icu_sort_key_gen : public labench {
    utf8_string_collator col;
    std::vector<icu::UnicodeString> icu_strings;
    std::vector<std::vector<uint8_t>> sort_keys;

    icu_sort_key_gen(const std::vector<std::string>& input) {
        icu_strings.reserve(input.size());
        for( const auto& s : input )
            icu_strings.emplace_back(icu::UnicodeString::fromUTF8(s));
    }

    void run() override {
        sort_keys.reserve(icu_strings.size());
        for( const auto& s : icu_strings )
            sort_keys.emplace_back(col.get_sort_key(&s));
    }
};

int main(int argc, char* argv[]) {
    if( argc != 2 ) {
        fprintf(stderr, "Pass string file path as only argument\n");
        std::exit(EXIT_FAILURE);
    }

    file_lines_to_strvec input;
    input.read(argv[1]);

    printf("String lines: %u short, %u long\n", input.short_strings, input.long_strings);

    {
        std::minstd_rand r;
        r.seed(0);
        shuffle(input.lines.begin(), input.lines.end(), &r);
    }

    std::vector<std::string> work;

    strcmp_comparator().setup_and_bench(input.lines, work);
    strcmp_on_raw_ptr_vec(input.lines).benchmark();
    strcmp_on_string_view_vec(input.lines).benchmark();
    strcmp_on_unique_ptr_vec(input.lines).benchmark();
    strcmp_on_reorder_vec(input.lines).benchmark();
    memcmp_comparator().setup_and_bench(input.lines, work);
    len_memcmp_comparator().setup_and_bench(input.lines, work);
    stdcmp_comparator().setup_and_bench(input.lines, work);
    icu_utf8_comparator().setup_and_bench(input.lines, work);
    icu_sort_key_gen(input.lines).benchmark();

    return 0;
}
