#include "common_utils.h"

#include <unicode/coll.h>
#include <unicode/locid.h>
#include <unicode/unistr.h>
#include <unicode/ures.h>
#include <unicode/utypes.h>

#include <cstdlib>
#include <cstring>

void file_line_reader::read(FILE* file)
{
    std::string tmp;
    const unsigned short_string_len = std::string().capacity();

    while( true ) {
        char buf[8*1024];
        size_t next_start = 0;
        const size_t r = fread(buf, 1, std::size(buf), file);
        if( r == 0 )
            break;

        for( size_t i = 0; i < r; ++i ) {
            if( buf[i] != '\n' )
                continue;

            const auto start = next_start;
            next_start = i + 1;

            std::string line;
            if( tmp.empty() ) {
                if( start == i )
                    continue;
                line = std::string(buf + start, buf + i);
            } else {
                line.reserve(tmp.size() + i - start);
                line = tmp;
                line.append(buf + start, buf + i);
                tmp.clear();
            }

            if( line.size() <= short_string_len )
                ++short_strings;
            else
                ++long_strings;

            (*this)(std::move(line));
        }

        if( next_start < r )
            tmp.append(buf + next_start, buf + r);
    }

    if( ferror(stdin) ) {
        perror("Error reading stdin");
        std::exit(EXIT_FAILURE);
    }

    if( !tmp.empty() )
        (*this)(std::move(tmp));
}

void file_line_reader::read(const char* file_path)
{
    FILE* f = fopen(file_path, "rb");
    if( f == nullptr ) {
        perror("Error opening file");
        std::exit(EXIT_FAILURE);
    }
    read(f);
    fclose(f);
}


utf8_string_collator::utf8_string_collator(const char* locale)
{
    UErrorCode status = U_ZERO_ERROR;

    auto res = ures_openDirect(nullptr, locale, &status);
    if( U_FAILURE(status) || res == nullptr ) {
        fprintf(stderr, "Failed to open resource for locale %s: %s\n", locale, u_errorName(status));
        std::exit(EXIT_FAILURE);
    }

    if( const auto actual = ures_getLocaleByType(res, ULOC_ACTUAL_LOCALE, &status); std::strcmp(locale, actual) != 0 ) {
        fprintf(stderr, "Failed to find exact matching resource for locale %s - got %s instead\n",
                locale, actual);
        std::exit(EXIT_FAILURE);
    }

    auto collator = icu::Collator::createInstance(icu::Locale::createFromName(locale), status);
    if( U_FAILURE(status) ) {
        fprintf(stderr, "Failed to create collator: %s\n", u_errorName(status));
        std::exit(EXIT_FAILURE);
    }
    collator->setAttribute(UCOL_STRENGTH, UCOL_IDENTICAL, status);
    if( U_FAILURE(status) ) {
        fprintf(stderr, "Failed to set collator strength: %s\n", u_errorName(status));
        std::exit(EXIT_FAILURE);
    }

    icu_res = res;
    icu_collator = collator;

    buffer.resize(1024);
}

utf8_string_collator::~utf8_string_collator()
{
    delete reinterpret_cast<icu::Collator*>(icu_collator);
    ures_close(reinterpret_cast<UResourceBundle*>(icu_res));
}

std::vector<uint8_t> utf8_string_collator::get_sort_key(const void* icuUnicodeString)
{
    auto s = reinterpret_cast<const icu::UnicodeString *>(icuUnicodeString);
    auto col = reinterpret_cast<icu::Collator*>(icu_collator);
    auto size = col->getSortKey(*s, buffer.data(), buffer.size());
    if( size_t(size) > buffer.size() ) {
        buffer.resize(size_t(size));
        col->getSortKey(*s, buffer.data(), buffer.size());
    }

    return std::vector<uint8_t>(buffer.begin(), buffer.begin() + size);
}

bool utf8_string_collator::comparator::operator()(const std::string& lhs, const std::string& rhs)
{
    UErrorCode status = U_ZERO_ERROR;
    bool result = reinterpret_cast<icu::Collator*>(icu_collator)->compareUTF8(lhs, rhs, status) == UCOL_LESS;
    if( U_FAILURE(status) ) {
        fprintf(stderr, "Failed to set compare: %s\n", u_errorName(status));
        std::exit(EXIT_FAILURE);
    }
    return result;
}
