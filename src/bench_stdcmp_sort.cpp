#include <common_utils.h>
#include <labench.h>

#include <gfx/timsort.hpp>
#include <pdqsort.h>
#include <ska_sort.hpp>

#include <algorithm>
#include <random>

static std::vector<std::string> sorted;

struct common_bench : public labench {
    std::vector<std::string>* work = nullptr;

    void setup_and_bench(const std::vector<std::string>& input, std::vector<std::string>& w) {
        w = input;
        work = &w;
        benchmark();
        if( *work != sorted )
            std::abort();
    }
};

struct cppsort_bench : public common_bench {
    void run() override {
        std::sort(work->begin(), work->end());
    }
};

struct timsort_bench : public common_bench {
    void run() override {
        gfx::timsort(work->begin(), work->end());
    }
};

struct pdqsort_bench : public common_bench {
    void run() override {
        pdqsort(work->begin(), work->end());
    }
};

struct skasort_bench : public common_bench {
    void run() override {
        ska_sort(work->begin(), work->end());
    }
};

static void run_benchmarks(const std::vector<std::string>& input, std::vector<std::string>& w)
{
    cppsort_bench().setup_and_bench(input, w);
    timsort_bench().setup_and_bench(input, w);
    pdqsort_bench().setup_and_bench(input, w);
    skasort_bench().setup_and_bench(input, w);
}


int main(int argc, char* argv[]) {
    if( argc != 2 ) {
        fprintf(stderr, "Pass string file path as only argument\n");
        std::exit(EXIT_FAILURE);
    }

    file_lines_to_strvec input;
    input.read(argv[1]);

    std::sort(input.lines.begin(), input.lines.end());
    sorted = input.lines;
    std::minstd_rand r;
    r.seed(0);

    std::vector<std::string> work;
    const auto N = input.lines.size();

    printf("Already sorted:\n");
    run_benchmarks(input.lines, work);

    printf("Reversed:\n");
    std::reverse(input.lines.begin(), input.lines.end());
    run_benchmarks(input.lines, work);

    printf("Random tail:\n");
    std::reverse(input.lines.begin(), input.lines.end());
    shuffle(input.lines.begin() + (N - (N / 10)), input.lines.end(), &r);
    run_benchmarks(input.lines, work);

    printf("Random half:\n");
    shuffle(input.lines.begin() + (N / 2), input.lines.end(), &r);
    run_benchmarks(input.lines, work);

    printf("Random:\n");
    shuffle(input.lines.begin(), input.lines.end(), &r);
    run_benchmarks(input.lines, work);

    printf("Nearly sorted:\n");
    input.lines = sorted;
    for( size_t i = 0; i < N / 100; ++i ) {
        using std::swap;
        const auto j = uniform_random_index(N, &r);
        const auto k = uniform_random_index(N, &r);
        swap(input.lines[j], input.lines[k]);
    }
    run_benchmarks(input.lines, work);

    return 0;
}
