#ifndef SSLBS_UTILS_H
#define SSLBS_UTILS_H

#include <cassert>
#include <limits>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>


struct file_line_reader {
    unsigned short_strings = 0;
    unsigned long_strings = 0;

    void read(FILE* file);
    void read(const char* file_path);
    virtual void operator()(std::string&& line) = 0;
};

struct file_lines_to_strvec : public file_line_reader {
    std::vector<std::string> lines;

    void operator ()(std::string&& line) override { lines.push_back(std::move(line)); }
};


template< typename UniformRNG >
uint32_t uniform_random_index(uint32_t count, UniformRNG* urng)
{
    constexpr auto urng_min = UniformRNG::min();
    constexpr auto urng_max = UniformRNG::max();

    static_assert(urng_max <= std::numeric_limits<uint64_t>::max(), "Unsupported UniformRNG range");
    constexpr uint64_t urng_d = urng_max - urng_min;

    assert(count > 0);
    const auto d = count - 1;

    if( urng_d > d ) {
        const uint64_t scaling = (urng_d == std::numeric_limits<uint64_t>::max() ? urng_d : urng_d + 1) / count;
        const uint64_t limit = count * scaling;
        uint64_t r;
        do {
            r = (*urng)() - urng_min;
        } while (r >= limit);
        return uint32_t(r / scaling);
    } else if( urng_d < d ) {
        constexpr uint32_t urng_range = uint32_t(urng_d) + 1;
        const uint32_t high = (d / urng_range) + 1;
        uint32_t tmp;
        uint32_t r;
        do {
            tmp = urng_range * uniform_random_index(high, urng);
            r = tmp + uint32_t((*urng)() - urng_min);
        } while( r >= count || r < tmp );
        return r;
    } else {
        return uint32_t((*urng)() - urng_min);
    }
}

template< typename RandomIt, typename UniformRNG >
void shuffle(RandomIt first, RandomIt last, UniformRNG* urng)
{
    const auto n = last - first;
    assert(n <= std::numeric_limits<uint32_t>::max());
    for( auto i = n - 1; i > 0; --i ) {
        using std::swap;
        swap(first[i], first[uniform_random_index(i + 1, urng)]);
    }
}

class utf8_string_collator {
public:
    utf8_string_collator(const char* locale = "en_US");
    ~utf8_string_collator();

    struct comparator {
        bool operator()(const std::string& lhs, const std::string& rhs);
        void* icu_collator = nullptr;
    };

    comparator get_comparator() { return {icu_collator}; }

    std::vector<uint8_t> get_sort_key(const void* icuUnicodeString);

    utf8_string_collator(const utf8_string_collator&) = delete;
    utf8_string_collator& operator=(const utf8_string_collator&) = delete;

private:
    void* icu_res = nullptr;
    void* icu_collator = nullptr;
    std::vector<uint8_t> buffer;
};

#endif
